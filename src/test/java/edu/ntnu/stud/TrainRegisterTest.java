package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.stud.models.TrainDeparture;
import edu.ntnu.stud.models.TrainRegister;
import edu.ntnu.stud.util.Util;
import java.time.LocalTime;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class TrainRegisterTest {

  private TrainRegister trainRegister;

  @BeforeEach
  public void setUp() {
    trainRegister = new TrainRegister();
  }

  @Nested
  @DisplayName("Adding and removing train departures")
  public class AddAndRemoveTrainDepartureTests {

    @Test
    @DisplayName("Adding a train departure")
    public void testAddTrainDeparture() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5");

      assertTrue(trainRegister.addTrainDeparture(trainDeparture));
      assertNotNull(trainRegister.getTrainNumber("5"));
      assertEquals(trainDeparture, trainRegister.getTrainNumber("5"));

    }

    @Test
    @DisplayName("Adding a train departure with duplicate train number should fail")
    public void testAddTrainDepartureDuplicate() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5");

      assertTrue(trainRegister.addTrainDeparture(trainDeparture));

      TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.of(3, 30),
                      "Oslo",
                      "B25  ",
                      "5");

      assertFalse(trainRegister.addTrainDeparture(trainDeparture2));
    }

    @Test
    @DisplayName("Removing existing train departure")
    public void testRemoveTrainDeparture() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5");
      trainRegister.addTrainDeparture(trainDeparture);

      assertTrue(trainRegister.removeTrainDeparture("5"));
      assertNull(trainRegister.getTrainNumber("5"));
    }

    @Test
    @DisplayName("Removing non-existing train departure should fail")
    public void testRemoveNonExistingTrainDeparture() {
      assertFalse(trainRegister.removeTrainDeparture("5"));
    }

    @Test
    @DisplayName("Removing all train departures")
    public void testRemoveAllTrains() {

      trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5"));
      trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              "6"));
      trainRegister.removeAllTrains();

      String trainTable = trainRegister.getTrainDeparturesTable();

      assertTrue(trainTable.trim().endsWith(Util.getTableSeparator().trim()));
    }
  }

  @Nested
  @DisplayName("Gets train number tests")
  public class GetTrainNumberTests {

    @Test
    @DisplayName("Gets existing train number")
    public void testGetExistingTrainNumber() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5");
      trainRegister.addTrainDeparture(trainDeparture);

      assertNotNull(trainRegister.getTrainNumber("5"));
      assertEquals(trainDeparture, trainRegister.getTrainNumber("5"));
    }

    @Test
    @DisplayName ("Gets non-existing train number")
    public void testGetNonExistingTrainNumber() {
      assertNull(trainRegister.getTrainNumber("5"));
    }
  }

  @Nested
  @DisplayName("Gets train destinations tests")
  public class GetDestinationTests {

    @Test
    @DisplayName("Gets train departure destinations")
    public void testGetDestination() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5");
      TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "6");
      trainRegister.addTrainDeparture(trainDeparture);
      trainRegister.addTrainDeparture(trainDeparture2);

      List<TrainDeparture> trainDepartures = trainRegister.getDestination("Trondheim");

      assertNotNull(trainDepartures);
      assertEquals(2, trainDepartures.size());
      assertTrue(trainDepartures.contains(trainDeparture)
          && trainDepartures.contains(trainDeparture2));
    }

    @Test
    @DisplayName("Gets non-existing train departure destinations")
    public void testGetNonExistingDestination() {
      trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
                      "Oslo",
                      "C10",
                      "5"));
      List<TrainDeparture> trainDepartures = trainRegister.getDestination("Bergen");
      assertNotNull(trainDepartures);
      assertTrue(trainDepartures.isEmpty());
    }
  }

  @Nested
  @DisplayName("Update clock and checks track availability tests")
  public class UpdateClockAndTrackAvailabilityTests {
    @Test
    @DisplayName("Updates clock")
    public void testUpdateClock() {
      LocalTime newTime = LocalTime.of(3, 0);
      assertTrue(trainRegister.updateClock(newTime));
      assertEquals(newTime, trainRegister.getPresentTime());
    }

    @Test
    @DisplayName("Updates clock with invalid time should fail")
    public void testUpdateClockInvalidTime() {
      trainRegister.updateClock(LocalTime.of(6, 0));

      assertFalse(trainRegister.updateClock(LocalTime.of(5, 0)));
    }

    @Test
    @DisplayName("Checks track availability with no trains added")
    public void testTrackAvailabilityNoTrainsAdded() {
      assertTrue(trainRegister.getTrackAvailability(LocalTime.of(2, 30), 1));
    }

    @Test
    @DisplayName("Checks track availability if train has same departure time and track")
    public void testTrackAvailabilitySameDepartureTimeAndTrack() {
      trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
                      LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5",
                      1));
      assertFalse(trainRegister.getTrackAvailability(LocalTime.of(1, 30), 1));
    }

    @Test
    @DisplayName("Tracks are available if train has different departure time and track")
    public void testTrackAvailabilityDifferentDepartureTimeAndTrack() {
      trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
                      LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5",
                      1));
      assertTrue(trainRegister.getTrackAvailability(LocalTime.of(2, 30), 1));
    }

    @Test
    @DisplayName("Tracks are available if train has same departure time and different track")
    public void testTrackAvailabilitySameDepartureTimeDifferentTrack() {
      trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
                      LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5",
                      1));
      assertTrue(trainRegister.getTrackAvailability(LocalTime.of(1, 30), 2));
    }

    @Nested
    @DisplayName("Train Table tests")
    public class TrainTableTests {
      @Test
      @DisplayName("Gets train table")
      public void testGetTrainTable() {
        trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
                        "Trondheim",
                        "C10",
                        "5"));
        trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
                        "Trondheim",
                        "C10",
                        "6"));

        trainRegister.updateClock(LocalTime.of(1, 0));

        String trainTable = trainRegister.getTrainDeparturesTable();
        assertNotNull(trainTable);
        assertTrue(trainTable.contains("5") && trainTable.contains("6"));
      }
    }

    @Test
    @DisplayName("Gets expired train table")
    public void testGetExpiredTrainTable() {
      trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "5"));
      trainRegister.addTrainDeparture(new TrainDeparture(LocalTime.of(1, 30),
                      "Trondheim",
                      "C10",
                      "6"));

      trainRegister.updateClock(LocalTime.of(2, 30));

      String expiredTrainTable = trainRegister.getExpiredTrainsTable();
      assertNotNull(expiredTrainTable);
      assertTrue(expiredTrainTable.contains("5") && expiredTrainTable.contains("6"));
    }
  }
}