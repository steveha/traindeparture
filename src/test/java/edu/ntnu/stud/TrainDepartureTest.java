package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.models.TrainDeparture;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class TrainDepartureTest {

  @Nested
  @DisplayName("Testing TrainDeparture constructor")
  public class TestConstructor {

    @Test
    @DisplayName("Testing constructor with delay and track")
    public void testConstructorWithDelayAndTrack() {
      assertDoesNotThrow(() ->
              new TrainDeparture(LocalTime.of(1, 30),
                      LocalTime.of(0, 30),
                      "Trondheim",
                      "C10",
                      "5",
                      1));
    }

    @Test
    @DisplayName("Testing valid constructor with delay and track.")
    public void testValidConstructorWithDelayAndTrack() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
              LocalTime.of(0, 30),
              "Trondheim",
              "C10",
              "5",
              1);

      assertEquals(LocalTime.of(1, 30), trainDeparture.getDepartureTime());
      assertEquals(LocalTime.of(0, 30), trainDeparture.getDelay());
      assertEquals("Trondheim", trainDeparture.getDestination());
      assertEquals("C10", trainDeparture.getLine());
      assertEquals("5", trainDeparture.getTrainNumber());
      assertEquals(1, trainDeparture.getTrack());
      assertNotNull(trainDeparture);
    }

    @Test
    @DisplayName("Testing valid constructor without delay and track")
    public void testValidConstructorWithoutDelayAndTrack() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              "5");

      assertEquals(LocalTime.of(1, 30), trainDeparture.getDepartureTime());
      assertNull(trainDeparture.getDelay());
      assertEquals("Trondheim", trainDeparture.getDestination());
      assertEquals("C10", trainDeparture.getLine());
      assertEquals("5", trainDeparture.getTrainNumber());
      assertEquals(-1, trainDeparture.getTrack());
      assertNotNull(trainDeparture);
    }

    @Test
    @DisplayName("Testing constructor without delay and track")
    public void testConstructorWithoutDelayAndTrack() {
      assertDoesNotThrow(() -> new TrainDeparture(LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              "5"));
    }

    @Test
    @DisplayName("Testing constructor set as default without track and delay")
    public void testConstructorSetAsDefaultWithoutTrackAndDelay() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              "5");

      assertNull(trainDeparture.getDelay());
      assertEquals(-1, trainDeparture.getTrack());
    }

    @Test
    @DisplayName("Testing constructor with null destination with throws exception")
    public void testConstructorNullDestination() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(1, 30),
              LocalTime.of(1, 30),
              null,
              "C10",
              "5",
              1));
    }

    @Test
    @DisplayName("Testing constructor with null line and throws exception")
    public void testConstructorNullLine() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(1, 30),
              LocalTime.of(1, 30),
              "Trondheim",
              null,
              "5",
              1));
    }

    @Test
    @DisplayName("Testing constructor with null train number and throws exception")
    public void testConstructorNullTrainNumber() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(1, 30),
              LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              null,
              1));
    }

    @Test
    @DisplayName("Testing constructor with blank destination and throws exception")
    public void testConstructorBlankDestination() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(1, 30),
              LocalTime.of(1, 30),
              "",
              "C10",
              "5",
              1));
    }

    @Test
    @DisplayName("Testing constructor with blank line and throws exception")
    public void testConstructorBlankLine() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(1, 30),
              LocalTime.of(1, 30),
              "Trondheim",
              "",
              "5",
              1));
    }

    @Test
    @DisplayName("Testing constructor with blank train number and throws exception")
    public void testConstructorBlankTrainNumber() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(LocalTime.of(1, 30),
              LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              "",
              1));
    }
  }

  @Nested
  @DisplayName("Testing negative track")
  public class TestNegativeTrack {

    @Test
    @DisplayName("Testing negative track")
    public void testSetNegativeTrack() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              "5");
      assertThrows(IllegalArgumentException.class, () -> trainDeparture.setTrack(-1));
    }
  }

  @Nested
  @DisplayName("Testing getters and setters")
  public class GettersAndSettersTest {
    private TrainDeparture traindeparture;

    @BeforeEach
    public void setUpObjectVariable() {
      LocalTime departureTime = LocalTime.of(1, 30);
      LocalTime delay = LocalTime.of(1, 30);
      String destination = "Trondheim";
      String line = "C10";
      String trainNumber = "5";
      int track = 1;

      traindeparture = new TrainDeparture(departureTime,
              delay,
              destination,
              line,
              trainNumber,
              track);
    }

    @Test
    @DisplayName("Testing getDepartureTime")
    public void testGetDepartureTime() {
      assertEquals(LocalTime.of(1, 30), traindeparture.getDepartureTime());
    }

    @Test
    @DisplayName("Testing getDelay")
    public void testGetDelay() {
      assertEquals(LocalTime.of(1, 30), traindeparture.getDelay());
    }

    @Test
    @DisplayName("Testing setter and getter for delay")
    public void testSetAndGetDelay() {
      traindeparture.setDelay(LocalTime.of(1, 45));
      assertEquals(LocalTime.of(1, 45), traindeparture.getDelay());
    }

    @Test
    @DisplayName("Testing setter and getter for track")
    public void setTrack() {
      traindeparture.setTrack(2);
      assertEquals(2, traindeparture.getTrack());
    }

    @Test
    @DisplayName("Testing getTrack")
    public void getTrack() {
      assertEquals(1, traindeparture.getTrack());
    }

    @Test
    @DisplayName("Testing getTrainNumber")
    public void testGetTrainNumber() {
      assertEquals("5", traindeparture.getTrainNumber());
    }

    @Test
    @DisplayName("Testing getDestination")
    public void testGetDestination() {
      assertEquals("Trondheim", traindeparture.getDestination());
    }

    @Test
    @DisplayName("Testing getLine")
    public void testGetLine() {
      assertEquals("C10", traindeparture.getLine());
    }
  }

  @Nested
  @DisplayName("Testing toString")
  public class ToStringTests {
    private TrainDeparture trainDeparture;

    @BeforeEach
    public void setUpObjectVariable() {
      LocalTime departureTime = LocalTime.of(1, 30);
      LocalTime delay = LocalTime.of(1, 30);
      String destination = "Trondheim";
      String line = "C10";
      String trainNumber = "5";
      int track = 1;

      trainDeparture = new TrainDeparture(departureTime,
              delay,
              destination,
              line,
              trainNumber,
              track);
    }

    @Test
    @DisplayName("Testing toString")
    public void testToString() {
      assertEquals("Departure Time: 01:30, Line: C10, Train Number: 5,"
              + " Destination: Trondheim, Delay: 01:30, Track: 1", trainDeparture.toString());
    }

    @Test
    @DisplayName("Testing toString with no delay and track")
    public void testToStringWithNoDelayAndTrack() {
      TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              "5");
      assertEquals("Departure Time: 01:30, Line: C10, Train Number: 5,"
              + " Destination: Trondheim", trainDeparture2.toString());
    }
  }

  @Nested
  @DisplayName("Testing equals and hashCode")
  public class EqualsAndHashCodeTests {

    @Test
    @DisplayName("Testing equals and hashCode with same train number")
    public void testEqualsAndHashCode() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              "5");

      TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.of(1, 30),
              "Oslo",
              "B5",
              "5");
      assertEquals(trainDeparture, trainDeparture2);
      assertEquals(trainDeparture.hashCode(), trainDeparture2.hashCode());

    }

    @Test
    @DisplayName("Testing equals and hashCode with different train number")
    public void testEqualsAndHashCodeWithDifferentTrainNumber() {
      TrainDeparture trainDeparture = new TrainDeparture(LocalTime.of(1, 30),
              "Trondheim",
              "C10",
              "5");

      TrainDeparture trainDeparture2 = new TrainDeparture(LocalTime.of(1, 30),
              "Oslo",
              "B5",
              "6");
      assertNotEquals(trainDeparture, trainDeparture2);
      assertNotEquals(trainDeparture.hashCode(), trainDeparture2.hashCode());
    }
  }
}