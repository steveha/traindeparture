package edu.ntnu.stud;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import edu.ntnu.stud.util.Util;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class UtilTest {
  @Nested
  @DisplayName("Testing letterFormat")
  public class LetterFormatTest {

    @Test
    @DisplayName("Testing letter format")
    public void testLetterFormat() {
      assertEquals("A", Util.letterFormat("a"));
    }

    @Test
    @DisplayName("Testing letter format with multiple letters")
    public void testLetterFormatWithMultipleLetters() {
      assertEquals("Abc", Util.letterFormat("abc"));
    }

    @Test
    @DisplayName("Testing letter format with null")
    public void testLetterFormatWithNull() {
      assertNull(null, Util.letterFormat(null));
    }

    @Test
    @DisplayName("Testing letter format with blank string")
    public void testLetterFormatWithBlankString() {
      assertEquals("", Util.letterFormat(""));
    }
  }

  @Nested
  @DisplayName("Testing getTableHeader and getTableSeparator")
  public class GetTableHeaderAndGetTableSeparatorTest {

    @Test
    @DisplayName("Testing getTableHeader")
    public void testGetTableHeader() {
      assertEquals("| Departure Time   | Line                | Train Number "
              + "| Destination   | Delay  | Track |", Util.getTableHeader());
    }

    @Test
    @DisplayName("Testing getTableSeparator")
    public void testGetTableSeparator() {
      assertEquals("+------------------+---------------------+--------------"
              + "+---------------+--------+-------+", Util.getTableSeparator());
    }
  }
}
