package edu.ntnu.stud.models;

import edu.ntnu.stud.util.Util;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The TrainRegister class is a registry responsible for managing train departures.
 * <p>
 *   The purpose of this class is to handle the logic for managing train departures,
 *   along with filtering and sorting them.
 *   Includes several methods for adding, removing, updating and getting train departures.
 *   The reason for this class is to separate the logic from the user interface, which
 *   provides a better overview of the code and makes it easier to maintain.
 *   The lists are all stored in a private field for encapsulation, and methods
 *   that interact with the lists are public.
 *   Also the list only allows for objects of TrainDeparture to be added to the list.
 *   Clock is set to 00:00 by default, and is updated when the updateClock method is called.
 * </p>
 *
 * @version v1.8.0
 * @since v0.5.0
 * @author steveha
 *
 */

public class TrainRegister {
  private final List<TrainDeparture> trainRegister = new ArrayList<>();
  private final List<TrainDeparture> expiredTrainRegister = new ArrayList<>();
  private LocalTime presentTime = LocalTime.of(0, 0);

  /**
   * Sorted list of train departures.
   * <p>
   *   The purpose of having a list of train departures is to store, and manage train departures.
   *   Takes the existing list trainRegister which contains train departures,
   *   and sorts them in an ascending order of departure time.
   *   Furthermore it then collects it to a new list and returns the sorted list.
   * </p>
   *
   * @return List of sorted train departures, sorted by departure time.
   */

  private List<TrainDeparture> getSortedTrainDeparture() {
    return trainRegister.stream()
            .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
            .collect(Collectors.toList());
  }

  /**
   * Sorted list of expired train departures.
   * <p>
   *   Similar to the getSortedTrainDeparture method,
   *   but instead it takes the expiredTrainRegister list.
   *   And sorts them in an ascending order of departure time.
   * </p>
   *
   * @see #getSortedTrainDeparture()
   * @return List of sorted expired train departures, sorted by departure time.
   */

  private List<TrainDeparture> getSortedExpiredTrainDeparture() {
    return expiredTrainRegister.stream()
            .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
            .collect(Collectors.toList());
  }

  /**
   * Adds a train departure to the train register.
   * <p>
   *   The way this works is that it checks if the trainRegister list
   *   contains the train departure. If it does, it returns false, and
   *   if it does not, it adds the train departure to the trainRegister list.
   *   Works in collaboration with the equals method in the TrainDeparture class.
   *   Also this uses aggregation instead of composition.
   * </p>
   *
   * @param trainDeparture The train departure to be added.
   * @return True if the train departure was added, false if it already exists in registry.
   */

  public boolean addTrainDeparture(TrainDeparture trainDeparture) {

    if (trainRegister.contains(trainDeparture)) {
      return false;
    }
    trainRegister.add(trainDeparture);
    return true;

  }

  /**
   * Removes a train departure from the trainRegister list.
   * <p>
   *   Filters out the list trainRegister based on train number,
   *   with the use of a lambda expression.
   *   Then adds the filtered list to the expiredTrainRegister list.
   *   And removes the train departure from the trainRegister list.
   *   Furthermore it returns false if the train departure does not exist,
   *   and returns true if the train departure was removed.
   *   Useful for removing certain train departures.
   * </p>
   *
   * @param trainNumber The train number of the train departure to be removed.
   * @return True if the train departure was removed, false otherwise.
   */

  public boolean removeTrainDeparture(String trainNumber) {
    List<TrainDeparture> toRemove = trainRegister.stream()
            .filter(e ->
                    e.getTrainNumber()
                            .equals(trainNumber)).toList();

    expiredTrainRegister.addAll(toRemove);
    return trainRegister.removeAll(toRemove);
  }

  /**
   * Removes all train departures from the train register.
   * <p>
   *   Once this method is called, it removes all train departures from the trainRegister list.
   *   Thereafter it adds all train departures to the expired train register,
   *   as they are now expired.
   *   The purpose of this is to remove all existing train departures if needed.
   * </p>
   *
   */

  public void removeAllTrains() {
    expiredTrainRegister.addAll(trainRegister);
    trainRegister.clear();
  }

  /**
   * Checks if a track is available at a given time.
   * <p>
   *   Uses a lambda expression to check if the departure time and track matches
   *   with the given departure time and track in the parameters.
   *   Streams through the trainRegister list and checks if the departure time and track matches.
   *   Will return true if the track is available, which indicates
   *   that there is no train departure at the given time and track.
   *   The purpose of this is to check if a track is available at a given time
   *   so that a train departure can be added.
   *  </p>
   *
   * @param departureTime The departure time of the train departure to be checked.
   * @param track The track of the train departure to be checked.
   * @return True if the track is available, false otherwise.
   */


  public boolean getTrackAvailability(LocalTime departureTime, int track) {
    return trainRegister.stream()
            .noneMatch(e ->
                    e.getDepartureTime()
                            .equals(departureTime) && e.getTrack() == track);
  }

  /**
   * Involves retrieving the first train departure by train number in a list.
   * <p>
   *   Streams the list trainRegister and filters out the list based on train number.
   *   It returns the train departure if trainNumber matches.
   *   However in situations where the given train number does not exist it will return null.
   *   The purpose of this is to return the first train departure with the given train number.
   * </p>
   *
   * @param trainNumber The train number of the train departure to be returned.
   * @return The train departure with the given train number, null if it does not exist.
   */

  public TrainDeparture getTrainNumber(String trainNumber) {
    return trainRegister.stream()
            .filter(e -> e.getTrainNumber()
                    .equals(trainNumber))
                            .findFirst()
                                    .orElse(null);
  }

  /**
   * A list of train departures by destination.
   * <p>
   *   Streams the list trainRegister and filters out the list based on the destination.
   *   If the destination matches, it adds it to a list and returns the list.
   *   And in the case it does not match it returns an empty list.
   *   The purpose is to return a list of train departures with the given destination.
   * </p>
   *
   * @param destination The destination of the train departures to search for
   * @return List of train departures with the given destination, empty list if it does not exist.
   */

  public List<TrainDeparture> getDestination(String destination) {
    return trainRegister.stream()
            .filter(e ->
                    e.getDestination()
                            .equalsIgnoreCase(destination))
                                            .collect(Collectors.toList());
  }

  /**
   * Handles the logic for updating the clock.
   * <p>
   *   Checks if the new time is after the current time with an if-statement, and
   *   a boolean expression. If the new time is after the current time, it updates the clock.
   *   Stream the trainRegister list and filters out the list based on if the departure time
   *   is before the new time. Then adds the filtered list to the expiredTrainRegister list.
   *   Returns true if the clock was updated, false otherwise.
   * </p>
   *
   * @param newTime The new time to be set.
   * @return True if the clock was updated, false otherwise.
   */

  public boolean updateClock(LocalTime newTime) {
    if (newTime.isAfter(presentTime)) {
      presentTime = newTime;
      List<TrainDeparture> toRemove = trainRegister.stream()
              .filter(e ->
                      e.getDepartureTime()
                              .isBefore(newTime))
                                      .toList();
      expiredTrainRegister.addAll(toRemove);
      trainRegister.removeAll(toRemove);
      return true;
    } else {
      return false;
    }
  }

  /**
   * The current time in a 24-hour format.
   * <p>
   *   It will return the current time with the format (HH:MM).
   *   Purpose of this is to see the current time.
   * </p>
   *
   * @see #updateClock(LocalTime)
   * @return The current time.
   */

  public LocalTime getPresentTime() {
    return presentTime;
  }

  /**
   * Adds train departures to the train register.
   * <p>
   *   Adds predefined train departures to the train register.
   *   The train objects are created and added to the train register,
   *   here instead of in the user interface which is better for encapsulation,
   *   and loosens the coupling between the classes.
   *   The purpose of this is to have predefined train departures so that
   *   the user does not have to add them manually.
   * </p>
   */

  public void addTrainDepartures() {
    trainRegister.add(new TrainDeparture(
            LocalTime.of(10, 0),
            "Oslo",
            "L22",
            "1"));
    trainRegister.add(new TrainDeparture(
            LocalTime.of(10, 15),
            LocalTime.of(0, 15),
            "Trondheim",
            "B9",
            "2",
            1));
    trainRegister.add(new TrainDeparture(
            LocalTime.of(6, 30),
            "Bergen",
            "A35",
            "3"));
    trainRegister.add(new TrainDeparture(
            LocalTime.of(9, 15),
            LocalTime.of(20, 0),
            "Trondheim",
            "K98",
            "4",
            5));
    trainRegister.add(new TrainDeparture(
            LocalTime.of(15, 0),
            "Oslo",
            "C20",
            "5"));

  }

  /**
   * Builds the train table from the trainRegister list.
   * <p>
   *   Streams the sorted list from getSortedTrainDeparture method
   *   And converts it with the use of .map() to a toTrainTable format.
   *   The purpose of this is to have an overview of all
   *   the available trains in the format method from TrainDeparture class.
   *   This uses methods from Util class to format the table as well,
   *   and uses the getTableSeparator method to separate the table.
   *   With the use of the string builder, it appends the table separator,
   *   table header and the table itself.
   *   Then returns the string builder as a string.
   *  </p>
   *
   * @see #getSortedTrainDeparture()
   * @return The train departures table over available trains.
   */

  public String getTrainDeparturesTable() {
    StringBuilder tableBuilder = new StringBuilder();

    tableBuilder.append(Util.getTableSeparator()).append("\n");
    tableBuilder.append(Util.getTableHeader()).append("\n");
    tableBuilder.append(Util.getTableSeparator()).append("\n");

    getSortedTrainDeparture().stream()
            .map(TrainDeparture::toTrainTable)
            .forEach(tableBuilder::append);

    tableBuilder.append(Util.getTableSeparator()).append("\n");
    return tableBuilder.toString();
  }

  /**
   * Builds the expired train table from the expiredTrainRegister list.
   * <p>
   *   Streams the sorted list from getSortedExpiredTrainDeparture method
   *   and converts it with the use of .map() to a toTrainTable format.
   *   The purpose of this is to have an overview of all
   *   the expired trains in the format method from TrainDeparture class.
   *   Similar to the getTrainDeparturesTable method, except this
   *   is the list over expired trains.
   * </p>
   *
   * @see #getSortedExpiredTrainDeparture()
   * @return The expired trains table over expired trains.
   */

  public String getExpiredTrainsTable() {
    StringBuilder tableBuilder = new StringBuilder();

    tableBuilder.append(Util.getTableSeparator()).append("\n");
    tableBuilder.append(Util.getTableHeader()).append("\n");
    tableBuilder.append(Util.getTableSeparator()).append("\n");

    getSortedExpiredTrainDeparture().stream()
            .map(TrainDeparture::toTrainTable)
            .forEach(tableBuilder::append);

    tableBuilder.append(Util.getTableSeparator()).append("\n");
    return tableBuilder.toString();
  }
}