package edu.ntnu.stud.models;

import java.time.LocalTime;
import java.util.Objects;

/**
 * Class TrainDeparture is representing a train departure.
 * <p>
 *   Responsible for representing a train departure.
 *   A train departure is a train that is departing from a station and
 *   has a departure time, destination, line, train number and track.
 *   It can also have a delay, which is the time the train is delayed.
 *   Attributes that use LocalTime are departureTime and delay, and
 *   is represented in the time format (HH:MM).
 *   The class is not completely immutable as it has setters for delay and track.
 *   This also includes two different constructor, where one includes
 *   delay and track, and the other does not. This way, when an object is created
 *   and the delay and track is not known, the constructor without delay and track is used.
 * </p>
 *
 * @version v2.1.1
 * @since v0.1.0
 * @author steveha
 *
 */

public class TrainDeparture {
  private final LocalTime departureTime;
  private LocalTime delay;
  private final String destination;
  private final String line;
  private final String trainNumber;
  private int track;

  /**
   * Constructor for TrainDeparture including delay and track.
   * <p>
   *   Constructor sets the departure time, delay, destination, line, train number and track.
   *   It is responsible for initializing the train departure.
   *   If destination, line or trainNumber are null or blank, an IllegalArgumentException is thrown.
   *   This ensures robustness of the application, and handles invalid input.
   * </p>
   *
   * @param departureTime The departure time of the train.
   * @param delay The delay of the train if there is any.
   * @param destination The destination of where the train is headed.
   * @param line The line of the train.
   * @param trainNumber The unique train number of the train.
   * @param track The track of the train from where it will depart.
   * @throws IllegalArgumentException If destination, line or trainNumber are null or blank.
   */


  public TrainDeparture(LocalTime departureTime,
                        LocalTime delay,
                        String destination,
                        String line,
                        String trainNumber,
                        int track) {
    if (destination == null || destination.isBlank()) {
      throw new IllegalArgumentException("invalid. Please enter a valid destination: ");
    }
    if (line == null || line.isBlank()) {
      throw new IllegalArgumentException("invalid. Please enter a valid line: ");
    }
    if (trainNumber == null || trainNumber.isBlank()) {
      throw new IllegalArgumentException("invalid. Please enter a valid train number: ");
    }

    this.departureTime = departureTime;
    this.delay = delay;
    this.destination = destination;
    this.line = line;
    this.trainNumber = trainNumber;
    this.track = track;
  }

  /**
   * Constructor for TrainDeparture without delay and track.
   * <p>
   *   Constructor sets the departure time, destination, line and train number.
   *   Responsible for initializing the train departure, and does not include track and delay.
   *   If destination, line or trainNumber are null or blank, an IllegalArgumentException is thrown.
   * </p>
   *
   * @param departureTime The departure time of the train.
   * @param destination The destination of where the train is headed.
   * @param line The line of the train.
   * @param trainNumber The unique train number of the train.
   * @throws IllegalArgumentException if destination, line or trainNumber are null or blank.
   */


  public TrainDeparture(LocalTime departureTime,
                        String destination,
                        String line,
                        String trainNumber) {
    if (destination == null || destination.isBlank()) {
      throw new IllegalArgumentException("invalid. Please enter a valid destination: ");
    }
    if (line == null || line.isBlank()) {
      throw new IllegalArgumentException("invalid. Please enter a valid line: ");
    }
    if (trainNumber == null || trainNumber.isBlank()) {
      throw new IllegalArgumentException("invalid. Please enter a valid train number: ");
    }


    this.departureTime = departureTime;
    this.delay = null;
    this.destination = destination;
    this.line = line;
    this.trainNumber = trainNumber;
    this.track = -1;

  }

  /**
   * Retrieves the departure time of the train.
   * Keyword this. is used to refer to the current object.
   *
   * @return departureTime of the train.
   */

  public LocalTime getDepartureTime() {
    return this.departureTime;
  }

  /**
   * Retrieves the delay of the train.
   *
   * @return the delay of the train.
   */

  public LocalTime getDelay() {
    return this.delay;
  }

  /**
   * Retrieves the destination of the train.
   *
   * @return destination of the train.
   */

  public String getDestination() {
    return this.destination;
  }

  /**
   * Retrieves the line of the train.
   *
   * @return the line of the train.
   */

  public String getLine() {
    return this.line;
  }

  /**
   * Retrieves the unique train number of the train.
   *
   * @return trainNumber of the train.
   */

  public String getTrainNumber() {
    return this.trainNumber;
  }

  /**
   * Retrieves the track of the train.
   *
   * @return track of the train.
   */

  public int getTrack() {
    return this.track;
  }

  /**
   * Sets the delay of the train.
   * <p>
   *   This will set the delay of the train.
   *   If null is passed as argument, an IllegalArgumentException is thrown,
   *   and if the delay is not null, it is set to the delay passed as an argument.
   * </p>
   *
   * @param delay The delay of the train.
   * @throws IllegalArgumentException If delay is null.
   */

  public void setDelay(LocalTime delay) throws IllegalArgumentException {
    if (delay == null) {
      throw new IllegalArgumentException("Invalid. Please enter a valid delay: ");
    } else {
      this.delay = delay;
    }
  }

  /**
   * Sets the track of the train.
   * <p>
   *   This will set the track of the train.
   *   If track is less than or equal to 0, an IllegalArgumentException is thrown,
   *   and if track is greater than 0, it is set to the track passed as an argument.
   * </p>
   *
   * @param track The track of the train.
   * @throws IllegalArgumentException If track is less than or equal to 0.
   */

  public void setTrack(int track) throws IllegalArgumentException {
    if (track <= 0) {
      throw new IllegalArgumentException("Invalid. Please enter a positive number: ");
    } else {
      this.track = track;
    }
  }

  /**
   * Displays the train table over available trains formatted to a string.
   * <p>
   *   The train table is formatted with the departure time, line, train number, destination,
   *   delay and track.
   *   If delay is null, it is displayed as blank in the table,
   *   also if the track is less than 0, it is displayed as blank in the table.
   *   The train table is formatted with the following format:
   * +------------------+---------------------+--------------+---------------+--------+-------+
   * | Departure Time   | Line                | Train Number | Destination   | Delay  | Track |
   * +------------------+---------------------+--------------+---------------+--------+-------+
   * | 06:30            | A35                 | 3            | Bergen        |        |       |
   * | 09:15            | K98                 | 4            | Trondheim     | 20:00  | 5     |
   * | 10:00            | L22                 | 1            | Oslo          |        |       |
   * | 10:15            | B9                  | 2            | Trondheim     | 00:15  | 1     |
   * | 15:00            | C20                 | 5            | Oslo          |        |       |
   * +------------------+---------------------+--------------+---------------+--------+-------+
   * </p>
   *
   * @return train table displaying information of the trains available.
   */

  public String toTrainTable() {
    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append(String.format("| %-16s | %-19s | %-12s | %-13s |",
            getDepartureTime(), getLine(), getTrainNumber(), getDestination()));

    if (getDelay() != null && !getDelay().equals(LocalTime.MIN)) {
      stringBuilder.append(String.format(" %-6s |", getDelay().toString()));
    } else {
      stringBuilder.append("        |");
    }

    if (getTrack() >= 0) {
      stringBuilder.append(String.format(" %-5d |", getTrack()));
    } else {
      stringBuilder.append("       |");
    }

    stringBuilder.append("\n");
    return stringBuilder.toString();
  }

  /**
   * Returns a string representation of the train departure object.
   * <p>
   *   The string representation of a single train departure.
   *   It is formatted with the departure time, line, train number, destination, delay and track.
   *   If delay is null or in the time format (00:00), it is not displayed, furthermore
   *   when track is less than 0, it is not displayed.
   *   The string representation of the train departure is formatted with the following format:
   *   Departure Time: 10:00, Line: L22, Train Number: 1, Destination: Oslo
   * </p>
   *
   * @return string representation of the train departure.
   */

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append("Departure Time: ").append(getDepartureTime())
            .append(", Line: ").append(getLine())
            .append(", Train Number: ").append(getTrainNumber())
            .append(", Destination: ").append(getDestination());

    if (delay != null && !delay.equals(LocalTime.MIN)) {
      sb.append(", Delay: ").append(delay);
    }

    if (track != -1) {
      sb.append(", Track: ").append(track);
    }

    return sb.toString();
  }

  /**
   * Checks if two train departures are equal.
   * This will check if two train departures are equal,
   * and is used in the TrainRegister class for comparing train numbers
   * to avoid duplicates.
   *
   * @param o The object to be compared.
   * @return true if the train departures are equal with same train number, false otherwise.
   */

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrainDeparture that = (TrainDeparture) o;
    return Objects.equals(trainNumber, that.trainNumber);
  }

  /**
   * Returns the hash code for this object.
   *
   * @return hash code for this object.
   */

  @Override
  public int hashCode() {
    return Objects.hash(trainNumber);
  }
}