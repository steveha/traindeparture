package edu.ntnu.stud.launcher;

import edu.ntnu.stud.userinterface.UserInterface;

/**
 * The main class for the train dispatch application.
 * <p>
 *   Class is responsible for starting the application, and contains the main method.
 *   The main method starts the application by calling init and start methods,
 *   from the UserInterface class, furthermore the init() initializes the application,
 *   and the start() starts the application.
 * </p>
 *
 * @version v1.2.0
 * @since v0.1.0
 * @author steveha
 *
 */
public class TrainDispatchApp {


  public static void main(String[] args) {
    UserInterface.init();
    UserInterface.start();
  }
}