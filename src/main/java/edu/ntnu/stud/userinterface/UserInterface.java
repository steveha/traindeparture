package edu.ntnu.stud.userinterface;

import edu.ntnu.stud.models.TrainDeparture;
import edu.ntnu.stud.models.TrainRegister;
import edu.ntnu.stud.util.Util;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Scanner;

/**
 * The class UserInterface is responsible for interacting with user.
 * <p>
 *   Interacts with user through the terminal, and handles user input.
 *   It interacts with the TrainRegister class depending on the tasks.
 *   The user is presented a menu with four options to choose from, and
 *   allows user to manage trains, update clock and display information table.
 *   The user menus are made with arrow switch, and not the traditional switch.
 *   With design principle encapsulation in mind, the attributes are made private static final,
 *   for immutability and methods made private static
 *   so that they are only accessible within the class,
 *   and able to be called by other methods within the class.
 *   Only the start and init methods are made public static so that
 *   you wont need to initialize the class to call the methods.
 *   For a user friendly experience, the user is in a loop that runs
 *   until the user chooses to quit the program.
 * </p>
 *
 * @version v.1.8.0
 * @since v.0.3.0
 * @author steveha
 *
 */
public class UserInterface {

  private static TrainRegister trainRegister;
  private static final String DESTINATION_REGEX = "^[a-zA-ZÅÆØåæø]+$";
  private static final String LINE_REGEX = "^[A-Za-zÅÆØåæø\\d]{1,3}$";
  private static final String NUMBER_REGEX = "[1-9]\\d{0,2}";

  private static final String INFORMATION_TABLE = "1";
  private static final String MANAGE_TRAINS = "2";
  private static final String SYSTEM_SETTINGS = "3";
  private static final String QUIT_PROGRAM = "4";

  private static final String SHOW_INFORMATION_TABLE = "1";
  private static final String SEARCH_TRAIN = "2";
  private static final String SEARCH_DESTINATION = "3";
  private static final String SHOW_EXPIRED_INFORMATION_TABLE = "4";
  private static final String RETURN_FROM_INFORMATION_TABLE = "5";

  private static final String ADD_TRAIN = "1";
  private static final String ADD_TRACK = "2";
  private static final String ADD_DELAY = "3";
  private static final String REMOVE_TRAIN = "4";
  private static final String REMOVE_ALL_TRAINS = "5";
  private static final String RETURN_FROM_MANAGE_TRAINS = "6";

  private static final String UPDATE_CLOCK = "1";
  private static final String RETURN_FROM_SYSTEM_SETTINGS = "2";

  /**
   * Initializes the application when called upon by the main method.
   * <p>
   *   Initializes the application by creating a new TrainRegister object.
   *   Then it calls the addTrainDepartures method from the TrainRegister class,
   *   which adds existing train departures to the registry.
   *   Useful for managing already existing trains instead of
   *   having to create a new one every time the program is started.
   * </p>
   */

  public static void init() {
    trainRegister = new TrainRegister();
    trainRegister.addTrainDepartures();
  }

  /**
   * Starts the application when called upon by the main method.
   * <p>
   *   Starts the application by calling the userMenu method, and displays the user menu.
   *   This method uses an arrow switch, and not the traditional switch because
   *   it is more readable and easier to understand. With the use of an arrow switch
   *   the code does not need to use break statements, and the code is more concise.
   *   The user has 4 options to choose from and is presented with the main menu.
   *   The main menu has options that send the user
   *   into smaller submenus for a better user experience and readability.
   *   Method displays viewing information table, managing trains, system settings and quit program.
   *   Prompts the user to enter a choice while the userMenu method is in do-while loop.
   *   This loop will continue until the user chooses to quit the program.
   * </p>
   *
   */

  public static void start() {

    Scanner scanner = new Scanner(System.in);
    String userInput;
    do {
      System.out.println("Current time: " + trainRegister.getPresentTime());
      userMenu();
      userInput = scanner.nextLine();

      switch (userInput) {
        case INFORMATION_TABLE -> informationTableMenu(scanner);
        case MANAGE_TRAINS -> manageTrainsMenu(scanner);
        case SYSTEM_SETTINGS -> systemSettingsMenu(scanner);
        case QUIT_PROGRAM -> quitProgram();
        default -> System.err.println("Invalid choice. Please try enter choice: (1-4) ");
      }
    } while (!userInput.equals(QUIT_PROGRAM));
    scanner.close();
  }

  /**
   * Displays the main menu.
   * <p>
   *   Here the user is presented with the main menu,
   *   and is presented with 4 options to choose from,
   *   depending on the chosen option it will send the user into submenus or quit the program.
   * </p>
   *
   */

  private static void userMenu() {
    System.out.println("Welcome to Train Dispatch System!");
    System.out.println("----------------------------");
    System.out.println(INFORMATION_TABLE + ". Information table");
    System.out.println(MANAGE_TRAINS + ". Manage trains");
    System.out.println(SYSTEM_SETTINGS + ". System settings");
    System.out.println(QUIT_PROGRAM + ". Quit program");
  }

  /**
   * Displays the information table submenu.
   * <p>
   *   The submenu from the main menu.
   *   An arrow switch is used, and the user has 5 options to choose from.
   *   It is responsible for displaying the information table,
   *   searching for train, searching for destination,
   *   displaying the expired information table and returning to the main menu.
   *   Prompts the user to enter a choice, and will continue to loop,
   *   until the user chooses to return to the main menu.
   * </p>
   *
   * @param scanner The scanner object for reading user input.
   */

  private static void informationTableMenu(Scanner scanner) {
    String userInput;
    boolean returnFromInformationTable = false;
    do {
      System.out.println("Information table");
      System.out.println("----------------------------");
      System.out.println(SHOW_INFORMATION_TABLE + ". Show information table");
      System.out.println(SEARCH_TRAIN + ". Search train");
      System.out.println(SEARCH_DESTINATION + ". Search destination");
      System.out.println(SHOW_EXPIRED_INFORMATION_TABLE + ". Show expired information table");
      System.out.println(RETURN_FROM_INFORMATION_TABLE + ". Return to main menu");
      userInput = scanner.nextLine();

      switch (userInput) {
        case SHOW_INFORMATION_TABLE -> showInformationTable();
        case SEARCH_TRAIN -> searchTrain(scanner);
        case SEARCH_DESTINATION -> searchDestination(scanner);
        case SHOW_EXPIRED_INFORMATION_TABLE -> showExpiredInformationTable();
        case RETURN_FROM_INFORMATION_TABLE -> returnFromInformationTable = true;
        default -> System.err.println("Invalid choice. Please enter choice: (1-5) ");
      }
    } while (!returnFromInformationTable);
  }

  /**
   * Displays the manage trains submenu.
   * <p>
   *   This is a submenu which uses an arrow switch,
   *   and not the traditional switch.
   *   The user has 6 options to choose from.
   *   It is responsible for adding trains, tracks and delays.
   *   With methods for removing trains, removing all trains and returning to the main menu.
   *   Prompts the user to enter a choice, and will continue to loop,
   *   until the user chooses to return to the main menu.
   * </p>
   *
   * @param scanner The scanner object for reading user input.
   */

  private static void manageTrainsMenu(Scanner scanner) {
    String userInput;
    boolean returnFromManageTrains = false;
    do {
      System.out.println("Manage trains");
      System.out.println("----------------------------");
      System.out.println(ADD_TRAIN + ". Add train");
      System.out.println(ADD_TRACK + ". Add track");
      System.out.println(ADD_DELAY + ". Add delay");
      System.out.println(REMOVE_TRAIN + ". Remove train");
      System.out.println(REMOVE_ALL_TRAINS + ". Remove all trains");
      System.out.println(RETURN_FROM_MANAGE_TRAINS + ". Return to main menu");
      userInput = scanner.nextLine();

      switch (userInput) {
        case ADD_TRAIN -> addTrain(scanner);
        case ADD_TRACK -> addTrack(scanner);
        case ADD_DELAY -> addDelay(scanner);
        case REMOVE_TRAIN -> removeTrain(scanner);
        case REMOVE_ALL_TRAINS -> removeAllTrains();
        case RETURN_FROM_MANAGE_TRAINS -> returnFromManageTrains = true;
        default -> System.err.println("Invalid choice. Please enter choice: (1-6) ");
      }
    } while (!returnFromManageTrains);
  }

  /**
   * Displays the system settings submenu.
   * <p>
   *   The last submenu from the main menu, and uses an arrow switch,
   *   and not the traditional switch.
   *   The user has 2 options to choose from.
   *   It is responsible for updating the clock.
   *   Prompts the user to enter a choice, and will continue to loop,
   *   until the user chooses to return to the main menu.
   * </p>
   *
   * @param scanner The scanner object for reading user input.
   */


  private static void systemSettingsMenu(Scanner scanner) {
    String userInput;
    boolean returnFromSystemSettings = false;
    do {
      System.out.println("System settings");
      System.out.println("----------------------------");
      System.out.println(UPDATE_CLOCK + ". Update clock");
      System.out.println(RETURN_FROM_SYSTEM_SETTINGS + ". Return to main menu");
      userInput = scanner.nextLine();

      switch (userInput) {
        case UPDATE_CLOCK -> updateClock(scanner);
        case RETURN_FROM_SYSTEM_SETTINGS -> returnFromSystemSettings = true;
        default -> System.err.println("Invalid choice. Please enter choice: (1-2) ");
      }
    } while (!returnFromSystemSettings);
  }

  /**
   * Displays the information table.
   * <p>
   *   Responsible for showing the information table to the user.
   *   Calls the getTrainDeparturesTable method from the TrainRegister class,
   *   which will show the departure time, line, train number, destination, delay and track.
   *   The information table is also sorted by the earliest departure time.
   *   Very useful for a user friendly experience, it is used throughout
   *   the program to show the user the current information table when
   *   wrong input is entered.
   * </p>
   */

  private static void showInformationTable() {
    System.out.println(trainRegister.getTrainDeparturesTable());
  }

  /**
   * Displays the expired information table.
   * <p>
   *   Responsible for showing a table of expired trains to the user, sorted
   *   by the earliest departure time.
   *   Calls the getExpiredTrainsTable method from the TrainRegister class,
   *   which shows the departure time, line, train number, destination, delay and track.
   *   Useful for when the user wants to see the expired trains.
   * </p>
   */

  private static void showExpiredInformationTable() {
    System.out.println("This is the expired information table: ");
    System.out.println(trainRegister.getExpiredTrainsTable());
  }

  /**
   * addTrain is responsible for adding a new train departure to the registry.
   * <p>
   *   The purpose of this is solely to add a new train departure to the registry.
   *   When this method is called it prompts the user to enter departure time,
   *   destination, line and train number with validations from the user input methods.
   *   AI has been used to ask if it was smart idea to separate the user input validations,
   *   rather than have it all in one method.
   *   Several user inputs are checked for validity, and if the user enters an invalid input,
   *   an error message will be printed and prompt the user to try again.
   *   The way the inputs are checked for validity is by using regex and try-catch blocks.
   *   More information regarding how validity is checked is documented in the given methods.
   *   Moreover if the train number exists, it will not add the train departure to the registry.
   * </p>
   *
   * @see #userInputDepartureTime(Scanner) for the departure time validation.
   * @param scanner The scanner object for reading user input.
   */

  private static void addTrain(Scanner scanner) {
    LocalTime departureTime = userInputDepartureTime(scanner);
    if (departureTime == null) {
      System.out.println("Exiting train addition process.");
      return;
    }


    String destination = userInputDestination(scanner);
    if (destination == null) {
      System.out.println("Exiting train addition process.");
      return;
    }

    String line = userInputLine(scanner);
    if (line == null) {
      System.out.println("Exiting train addition process.");
      return;
    }

    String trainNumber = userInputTrainNumberForAddTrain(scanner);
    if (trainNumber == null) {
      System.out.println("Exiting train addition process.");
      return;
    }

    try {
      TrainDeparture trainDeparture = new TrainDeparture(departureTime,
              destination,
              line,
              trainNumber);

      if (trainRegister.addTrainDeparture(trainDeparture)) {
        System.out.println("Train added: \n" + trainDeparture);
      } else {
        System.out.println("Entered train number already exists. " + "\n"
                + "Here is the current information table:");
        showInformationTable();
      }
    } catch (IllegalArgumentException e) {
      System.err.println("An error occurred: " + e.getMessage());
    }
  }

  /**
   * Handles the removal of a single train departure from the registry.
   * <p>
   *   Prompts the user to enter a train number to be removed.
   *   Scanner object is passed as a parameter to the method for reading user input.
   *   With scanner as a parameter it not only make the code more readable, but also
   *   makes it so you don't need to initialize the scanner object in the method.
   *   If the train number exists in the system, it will remove
   *   the train departure from the registry.
   *   However, if the train number does not exist in the system,
   *   it will print out an error message, along with the current information table.
   *   All validations are done in the userInputTrainNumber, and will
   *   continue to prompt the user to enter a valid train number with a loop.
   *   If the user enters 0, it will exit the train removal process.
   *  </p>
   *
   * @param scanner The scanner object for reading user input.
   */

  private static void removeTrain(Scanner scanner) {
    boolean removeTrain = false;

    while (!removeTrain) {
      String trainNumber = userInputTrainNumber(scanner);
      if (trainNumber == null) {
        System.out.println("Exiting train removal process.");
        return;
      }

      if (trainRegister.removeTrainDeparture(trainNumber)) {
        System.out.println("Train removed successfully.");
        removeTrain = true;
      } else {
        System.out.println("Train " + trainNumber + " not recognized in the system. " + "\n"
                + "Here is the current information table:");
        showInformationTable();
      }
    }
  }

  /**
   * Handles the removal of all train departures from the registry.
   * <p>
   *   Removes all the trains currently in the registry.
   *   After the procedure is done, it will print out a message
   *   saying that all trains has been removed.
   *   The user will then be presented with the current information table which is now empty.
   * </p>
   *
   */

  private static void removeAllTrains() {
    trainRegister.removeAllTrains();
    System.out.println("All trains removed successfully. " + "\n"
            + "Here is the current information table:");
    showInformationTable();
  }

  /**
   * AddTrack is responsible for the addition of a track to a train departure.
   * <p>
   *   Purpose of this is to add tracks to any existing train departures.
   *   Prompts the user to enter a train number and a track number.
   *   The way this is validated is by using the userInput methods.
   *   In cases where the train number and track number is valid,
   *   it will add the track to the train departure.
   *   However if the train number or track number is invalid,
   *   it will print out an error message, along with the current information table.
   *   The user is in a loop that will continue to prompt the user to enter a valid
   *   input with several guard conditions.
   *   Guard conditions is purposely done to make the code more readable and user friendly.
   *   At last it uses methods from the TrainRegister class
   *   to check for existing train number and track availability.
   * </p>
   *
   * @param scanner The scanner object for reading user input.
   */

  private static void addTrack(Scanner scanner) {
    boolean trackAdded = false;

    while (!trackAdded) {
      String trainNumber = userInputTrainNumber(scanner);
      if (trainNumber == null) {
        System.out.println("Exiting track update process.");
        return;
      }

      TrainDeparture trainDeparture = trainRegister.getTrainNumber(trainNumber);
      if (trainDeparture == null) {
        System.out.println("Train " + trainNumber + " not recognized in the system. " + "\n"
                + "Here is the current information table:");
        showInformationTable();
        continue;
      }

      Integer track = userInputTrack(scanner);
      if (track == null) {
        System.out.println("Exiting track update process.");
        return;
      }

      if (trainRegister.getTrackAvailability(trainDeparture.getDepartureTime(), track)) {
        trainDeparture.setTrack(track);
        System.out.println("Updated train departure: " + trainDeparture);
        trackAdded = true;
      } else {
        System.out.println("Track: " + track +  " is not available. " + "\n"
                + "Here is the current information table:");
        showInformationTable();
      }
    }
  }

  /**
   * AddDelay's purpose is to add delay to any existing train departures.
   * <p>
   *   Similar method to addTrack.
   *   Adds a delay to any existing train departure.
   *   Prompts the user to enter a train number and a delay.
   *   If the train number and delay is valid,
   *   it will add the delay to the train departure.
   *   In the cases that the user enters an invalid train number or delay,
   *   they will be looped back to the beginning of the method.
   *   The user will then be presented with the current information table.
   *   And the method will continue to loop until the user enters a valid train number,
   *   and delay or quits the process.
   * </p>
   *
   * @param scanner The scanner object for reading user input.
   */

  private static void addDelay(Scanner scanner) {
    boolean delayAdded = false;

    while (!delayAdded) {
      String trainNumber = userInputTrainNumber(scanner);
      if (trainNumber == null) {
        System.out.println("Exiting delay addition process.");
        return;
      }

      TrainDeparture trainDeparture = trainRegister.getTrainNumber(trainNumber);
      if (trainDeparture == null) {
        System.out.println("Train " + trainNumber + " not recognized in the system. " + "\n"
                + "Here is the current information table:");
        showInformationTable();
        continue;
      }

      LocalTime delay = userInputDelay(scanner);
      if (delay == null) {
        System.out.println("Exiting delay addition process.");
        return;
      }

      trainDeparture.setDelay(delay);
      System.out.println(trainDeparture);
      delayAdded = true;
    }
  }

  /**
   * Searches for a train departure by train number.
   * <p>
   *   Responsible for searching a train departure by train number,
   *   and uses method from TrainRegister class.
   *   This will ask the user to enter a train number,
   *   if the train number exists in the system, it will print out the train departure.
   *   Invalid train numbers will print out an error message,
   *   and the user will be presented with the current information table.
   *   If the user enters 0, it will exit the search process.
   * </p>
   *
   * @param scanner The scanner object for reading user input.
   */

  private static void searchTrain(Scanner scanner) {
    boolean trainFound = false;

    while (!trainFound) {
      String trainNumber = userInputTrainNumber(scanner);
      if (trainNumber == null) {
        System.out.println("Exiting search process.");
        return;
      }

      TrainDeparture trainDeparture = trainRegister.getTrainNumber(trainNumber);

      if (trainDeparture != null) {
        System.out.println(trainDeparture);
        trainFound = true;
      } else {
        System.out.println("Train " + trainNumber +  " not recognized in the system. " + "\n"
                + "Here is the current information table:");
        showInformationTable();

      }
    }
  }

  /**
   * Handles the search for a train departure by destination.
   * <p>
   *   Searches for a train departure by destination.
   *   This will ask the user to enter a destination.
   *   If the destination exists in the system, it will print out the train departure.
   *   Invalid destinations will print out an error message,
   *   and the user will be presented with the current information table.
   *   The way this is done is by first creating a temporary list of train departures,
   *   and then checking if the list is empty or not from the method
   *   getDestination in TrainRegister class which has a list of all the train departures.
   *   Uses a guard condition, similar to all methods involving
   *   user interaction in UserInterface class.
   * </p>
   *
   * @param scanner The scanner object for reading user input.
   */

  private static void searchDestination(Scanner scanner) {
    boolean destinationFound = false;

    while (!destinationFound) {
      String destination = userInputDestination(scanner);
      if (destination == null) {
        System.out.println("Exiting search process.");
        return;
      }

      List<TrainDeparture> trainDestination = trainRegister.getDestination(destination);
      if (!trainDestination.isEmpty()) {
        System.out.println("Trains to " + destination + ": ");
        trainDestination.forEach(System.out::println);
        destinationFound = true;

      } else {
        System.out.println("No trains to " + destination + " found. " + "\n"
                + "Here is the current information table:");
        showInformationTable();
      }
    }
  }

  /**
   * Updates the 24-hour clock.
   * <p>
   *   Asks the user to enter a new time, and
   *   If the new time is valid, the clock will be updated.
   *   Invalid times will print out an error message and the user will be prompted to try again.
   *   However when the user enters 0, it will exit the clock update process.
   *   The way this is looped is with a boolean and while loop, along with methods from
   *   TrainRegister class for updating the clock.
   *   The try-catch block is used for catching DateTimeParseException.
   * </p>
   *
   * @param scanner The scanner object for reading user input.
   */

  private static void updateClock(Scanner scanner) {
    boolean updatedTime = false;

    while (!updatedTime) {
      System.out.println("Current time: " + trainRegister.getPresentTime());
      System.out.println("Enter new time in a 24-hour format: (HH:MM) or 0 to exit:");

      String userInput = scanner.nextLine();

      if (userInput.equals("0")) {
        System.out.println("Exiting clock update process.");
        return;
      }

      try {
        LocalTime newTime = LocalTime.parse(userInput);
        updatedTime = trainRegister.updateClock(newTime);
        if (!updatedTime) {
          System.err.println("Time cannot be set to a time before the current time. " + "\n"
                  + "Please try again.");
        }
      } catch (Exception e) {
        System.err.println("Invalid. Please try again in a 24-hour format: (HH:MM): ");
      }
    }
  }

  /**
   * Responsible for user input validation for departure time.
   * <p>
   *   Its purpose is to make sure that the user input is valid.
   *   Especially useful when different methods need validations
   *   which is used throughout the program.
   *   It was implemented to separate the validations from the methods,
   *   which will create a cleaner, more organized and readable code.
   *   Handles invalid input by using a try-catch block for catching DateTimeParseException.
   *  </p>
   *
   * @see #addTrain(Scanner) for the addTrain method.
   * @param scanner The scanner object for reading user input.
   * @return The departure time if the user input is valid, null otherwise.
   */

  private static LocalTime userInputDepartureTime(Scanner scanner) {
    while (true) {
      System.out.println("Enter departure time in a 24-hour format: (HH:MM) or 0 to exit: ");
      String userInput = scanner.nextLine();
      if (userInput.equals("0")) {
        return null;
      }

      try {
        LocalTime departureTime = LocalTime.parse(userInput);
        LocalTime presentTime = trainRegister.getPresentTime();
        if (departureTime.isBefore(presentTime)) {
          System.err.println("Departure time cannot be set before the current time. " + "\n"
                  + "Please try again.");
        } else {
          return departureTime;
        }
      } catch (DateTimeParseException e) {
        System.err.println("Invalid. Please try again in a 24-hour format: (HH:MM): ");
      }
    }
  }

  /**
   * User input validation for destination.
   * <p>
   *   Similar to the userInputDepartureTime method.
   *   Its purpose is to make sure that the user input is valid,
   *   and that the destination only contains letters.
   *   Code will continue to loop until the user enters a valid destination.
   *   If the user enters 0, it will exit the train addition process.
   *   Uses method from Util class for capitalizing the first letter of the string.
   * </p>
   *
   * @see #userInputDepartureTime(Scanner) for the departure time validation.
   * @param scanner The scanner object for reading user input.
   * @return The destination if the user input is valid, null otherwise.
   */

  private static String userInputDestination(Scanner scanner) {
    while (true) {
      System.out.println("Enter destination or 0 to exit: ");
      String destination = scanner.nextLine();
      if (destination.equals("0")) {
        return null;
      }
      if (destination.isBlank() || !destination.matches(DESTINATION_REGEX)) {
        System.err.println("The entered destination is invalid. " + "\n"
                + "Please enter a valid destination (Only letters): ");
      } else {
        return Util.letterFormat(destination);
      }
    }
  }

  /**
   * User input validation for line.
   * <p>
   *   Similar to the userInputDestination method, but differs
   *   in the way that it allows letters and numbers, but also
   *   as the name of the method implies validates for line.
   *   Has a regex for checking if the user input is valid,
   *   which only allows letters and numbers up to 3 characters.
   *   With a while-loop, it will continue to loop until the user enters a valid line.
   * </p>
   *
   * @see #userInputDestination(Scanner) for the destination validation.
   * @param scanner The scanner object for reading user input.
   * @return The line if the user input is valid, null otherwise.
   */

  private static String userInputLine(Scanner scanner) {
    while (true) {
      System.out.println("Enter line or 0 to exit: ");
      String line = scanner.nextLine();
      if (line.equals("0")) {
        return null;
      }
      if (!line.matches(LINE_REGEX)) {
        System.err.println("The entered line is invalid. " + "\n"
                + "Please enter a valid line (Only letters and numbers of maximum 3 characters): ");
      } else {
        return Util.letterFormat(line);
      }
    }
  }

  /**
   * User input for train number.
   * <p>
   *   Similar to the userInputDestination method.
   *   The regex attribute allows numbers between 1 to 999.
   *   Code will continue to loop until the user enters a valid train number.
   *   If the user enters 0, it will exit the train addition process.
   * </p>
   *
   * @see #userInputDestination(Scanner) for the destination validation.
   * @param scanner The scanner object for reading user input.
   * @return The train number if the user input is valid, null otherwise.
   */

  private static String userInputTrainNumber(Scanner scanner) {
    while (true) {
      System.out.println("Enter train number or 0 to exit: ");
      String trainNumber = scanner.nextLine();
      if (trainNumber.equals("0")) {
        return null;
      }
      if (trainNumber.isBlank() || !trainNumber.matches(NUMBER_REGEX)) {
        System.err.println("The entered train number is invalid. " + "\n"
                + "Please enter a valid train number (Only numbers between 1 to 999): ");
      } else {
        return trainNumber;
      }
    }
  }

  /**
   * Handles user input for train number.
   * <p>
   *   Similar to the userInputTrainNumber method,
   *   but this method is specifically for addTrains method.
   *   This is used so that when the user prompts invalid train number, it will still loop.
   *   The regex allows numbers between 1 to 999.
   * </p>
   *
   * @see #userInputTrainNumber(Scanner) for the train number validation.
   * @param scanner The scanner object for reading user input.
   * @return The train number if the user input is valid, null otherwise.
   */

  private static String userInputTrainNumberForAddTrain(Scanner scanner) {
    while (true) {
      System.out.println("Enter train number or 0 to exit: ");
      String trainNumber = scanner.nextLine();
      if (trainNumber.equals("0")) {
        return null;
      }
      if (trainNumber.isBlank() || !trainNumber.matches(NUMBER_REGEX)) {
        System.err.println("The entered train number is invalid. " + "\n"
                + "Please enter a valid train number (Only numbers between 1 to 999): ");
      } else if (trainRegister.getTrainNumber(trainNumber) != null) {
        System.out.println("Entered train number already exists. " + "\n"
                + "Here is the current information table:");
        showInformationTable();
      } else {
        return trainNumber;
      }
    }
  }

  /**
   * User input for track.
   * <p>
   *   Similar to the userInputTrainNumber method.
   *   The regex allows numbers between 1 to 9.
   *   Code will continue to loop until the user enters a valid track.
   *   This is used so that when the user prompts invalid track, it will still loop.
   *   If the user enters 0, it will exit the track update process.
   * </p>
   *
   * @see #userInputTrainNumber(Scanner) for the train number validation.
   * @param scanner The scanner object for reading user input.
   * @return The track if the user input is valid, null otherwise.
   */

  private static Integer userInputTrack(Scanner scanner) {
    while (true) {
      System.out.println("Enter track or 0 to exit: ");
      String userInput = scanner.nextLine();
      if (userInput.equals("0")) {
        return null;
      }
      try {
        int track = Integer.parseInt(userInput);
        if (track <= 0 || track >= 10) {
          System.err.println("Track number must be between 1 to 9. Please try again: ");
        } else {
          return track;
        }
      } catch (NumberFormatException e) {
        System.err.println("Track number must be between 1 to 9. Please try again: ");
      }
    }
  }

  /**
   * User input for delay.
   * <p>
   *   Similar to the userInputDepartureTime method.
   *   Code will continue to loop until the user enters a valid delay in a 24-hour format.
   *   If the user enters 0, it will exit the delay addition process.
   * </p>
   *
   * @see #userInputDepartureTime(Scanner) for the departure time validation.
   * @param scanner The scanner object for reading user input.
   * @return The delay if the user input is valid, null otherwise.
   */

  private static LocalTime userInputDelay(Scanner scanner) {
    while (true) {
      System.out.println("Enter delay in a 24-hour format: (HH:MM) or 0 to exit: ");
      String userInput = scanner.nextLine();
      if (userInput.equals("0")) {
        return null;
      }
      try {
        return LocalTime.parse(userInput);
      } catch (DateTimeParseException e) {
        System.err.println("Invalid. Please try again in a 24-hour format (HH:MM): ");
      }
    }
  }

  /**
   * Quits the program if the user chooses to quit from the main menu.
   */

  private static void quitProgram() {
    System.out.println("Program ends... Have an amazing day!");
  }
}