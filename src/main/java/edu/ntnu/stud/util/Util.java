package edu.ntnu.stud.util;

/**
 * The Util class contains utility methods.
 * <p>
 *   This class is responsible for containing useful utility methods,
 *   such as methods for formatting letters and table headers.
 *   The purpose of this class is to contain methods that are used
 *   repeatedly throughout the application, which in turn
 *   increases the readability of the code and makes it easier to maintain.
 * </p>
 *
 * @version v2.2.0
 * @since v1.2.2
 * @author steveha
 *
 */
public class Util {

  /**
   * Creates a format for the header of the table.
   * <p>
   *   Adds a head row to the table, which includes the column names for:
   *   Departure Time, Line, Train Number, Destination, Delay and Track.
   *   Very useful for having a clean and organized table.
   *   The purpose of this is to make the table look more organized and clean.
   * </p>
   *
   * @return String prints out the header of the table.
   */

  public static String getTableHeader() {
    return "| Departure Time   | Line                | Train Number "
            + "| Destination   | Delay  | Track |";
  }

  /**
   * Creates a format for the separator of the table.
   * <p>
   *   Adds a separator row to the table, and is useful for
   *   creating a separator for the table making it look more organized.
   *   The purpose of this is also to make the table look more organized and clean.
   * </p>
   *
   * @return String prints out the separator of the table.
   */

  public static String getTableSeparator() {
    return "+------------------+---------------------+--------------"
            + "+---------------+--------+-------+";
  }

  /**
   * Formats how letters are displayed.
   * <p>
   *   Capitalizes the first letter of the string in upper case,
   *   and the rest of the letters in lower case.
   *   Very practical and useful for formatting letters for the user.
   *   If the string is null or blank, it will return the string as it is.
   *   The purpose of this is to make each letter look more organized and clean.
   * </p>
   *
   * @param input String to be formatted.
   * @return First letter of the string is capitalized in upper case.
   */

  public static String letterFormat(String input) {
    if (input == null || input.isBlank()) {
      return input;
    }
    return input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
  }
}