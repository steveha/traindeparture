# Portfolio project IDATT1003 - 2023

<strong>STUDENT NAME</strong> = Steven Ha <br>
<strong>STUDENT ID</strong> = 10036

## Project description

<p> This Java project is made with maven and is a task assignment for the course <strong>IDATT1003 - Programming 1 at NTNU.</strong></p>

The program involves creating a train dispatch system for managing trains.
Furthermore, several different methods are implemented for interacting with the trains such as adding new ones, removing, filtering, searching etc. 

## Project structure

<p>The <strong>source</strong> files of this project is as follows:</p>

The main class is located in the `src/main/java/edu/ntnu/stud/launcher` folder. <br>
TrainDeparture and TrainRegister classes are located in `src/main/java/edu/ntnu/stud/models` folder.<br>
The UserInterface class is located in the `src/main/java/edu/ntnu/stud/userinterface` folder.<br>
The Utils class is located in the `src/main/java/edu/ntnu/stud/util` folder. <br>
While all the JUnit test classes are in the `src/test/java/edu/ntnu/stud` folder.<br>

<p>Classes are in different individual packages for a more organized structure, and to make it easier to find the classes.
Packages have appropriate fitting names to make it easier to understand what the classes in the packages are about.</p>


## Link to repository

For more information see [here](https://gitlab.stud.idi.ntnu.no/steveha/traindeparture).

## How to run the project

After unzipping the zip folder, find the project folder `TrainDepartureSystem` and open the folder `IDATT1003-2023-Mappe-TrainDispatchSystem-Template`.
Once you have opened the correct folder `IDATT1003-2023-Mappe-TrainDispatchSystem-Template`, you now have access to the project, and git log history.<br>

To run this project you can use the terminal or any IDE of your choice, such as IntelliJ IDEA or Eclipse.
<strong>TrainDispatchApp</strong> is the main class in this project, and contains the main method.
Start the program from the main class, this will call upon the start() method from the UserInterface class.
The main class also has an init() method which adds 5 pre-existing trains to the train register so that the user can test the program without having to add trains manually.

The input as well as the output behaviour of the program uses <strong>command line interface (CLI)</strong> to interact with the user.
This means that the user can type in the terminal to interact with the program.
When the program starts, the user is presented with the main menu offering 4 different options to choose from.
These options send the user to different sub-menus where the user can choose from a variety of options.
The user can also quit the program at any time from the main menu by choosing the quit option.

For more information see [here](https://www.jetbrains.com/help/idea/run-java-applications.html).

## How to run the tests

<p>Before running tests <strong>JUnit 5</strong> needs to be installed in your IDE. Once this is installed in your choice of IDE, find the JUnit test classes within the project.</p>


After the test classes has been found, you can either run the whole test class or run the methods individually.
This is done by clicking the green play button next to the class or method name.
It will show you how many of the tests that has passed and failed,
which makes it easier to see if the tests are working as intended.
You can find the JUnit test classes in the `src/test/java/edu/ntnu/stud` folder.

For more information see [here](https://www.jetbrains.com/help/idea/performing-tests.html).
